# frontend

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development (and compiles svg's)
```
yarn dev
```

### Compiles and minifies for production
```
yarn run build
```









Introducing the first collaborative case management platform for litigators.

In January 2015 I joined Everchron as Lead Designer – since then I’m responsible for the web application design & front-end, iOS, and Android app designs as well as all marketing efforts such as the main corporate site.

In 2018, we launched our new corporate site to reflect the full functionality of the main product. I was responsible for design, front-end development and back-end development of the public site as well as some areas hidden behind a login for customers.

Most of the work I’ve done for Everchron isn’t disclosed to the public yet and is only available to current customers. This includes the main web application and the iOS and Android apps.

-----------------------

Scalable, web-based reservoir modelling and reservoir management for the oil and gas industry.

Resoptima specializes in scientific analysis of static and dynamic data to provide scalable software for reservoir modelling and reservoir management. I helped them design and develop a design system that powers all their web-based applications, and I designed the interface for a new reservoir modelling software.

The Resoptima design system was built from the ground up to fit every user interface's needs for Resoptima's web-based products. It now contains more than 100 highly flexible and fully responsive components.

Users can generate forecasts based on an ensemble of reservoir models, providing forecasts with a realistic view on uncertainty. Studies and scenarios are presented in an easy to filter view, grouped by oil fields.

Scenario creation is a process of collecting the correct data sets to run extensive simulations. An interactive table view allows users to filter, group, sort and edit control variables in bulk.

--------

FlarumPro is forum software that makes it easy to start and grow a successful online community.

The idea was
var path = require('path')

module.exports = {
	configureWebpack: {
		devtool: 'inline-source-map',
		resolve: {
			alias: {
				'include-media': path.join(process.cwd(), '/node_modules/include-media/dist/_include-media.scss'),
			}
		}
	},
	devServer: {
		proxy: {
			'/api/*': {
				target: 'http://localhost:4000',
				secure: false,
				pathRewrite: {'^/api': ''}
			}
		}
	}
}
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const withPrefix = (prefix, routes) =>
	routes.map(route => {
		route.path = prefix + route.path
		return route
	})

const title = 'Marcel Olszewski • Full Stack Developer/Designer'

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	scrollBehavior() {
		return { x: 0, y: 0 };
	},
	routes: [
		{
			path: '/',
			name: 'home',
			component: () => import(/* webpackChunkName: "Home" */ './views/Home/Home.vue'),
			meta: { title: title }
		},
		...withPrefix('/work', [
			{
				path: '/supaglobal',
				name: 'supaglobal',
				component: () => import(/* webpackChunkName: "Work" */ './views/Work/SupaGlobal/SupaGlobal.vue'),
				meta: { title: `SupaGlobal - ${title}` }
			},
			{
				path: '/parcel',
				name: 'parcel',
				component: () => import(/* webpackChunkName: "Work" */ './views/Work/Parcel/Parcel.vue'),
				meta: { title: `Parcel - ${title}` }
			},
			{
				path: '/flarumpro',
				name: 'flarumpro',
				component: () => import(/* webpackChunkName: "Work" */ './views/Work/FlarumPro/FlarumPro.vue'),
				meta: { title: `FlarumPro - ${title}` }
			},
			{
				path: '/brd',
				name: 'brd',
				component: () => import(/* webpackChunkName: "Work" */ './views/Work/BRD/BRD.vue'),
				meta: { title: `BRD - ${title}` }
			},
		]),
		{
			path: '/contact',
			name: 'contact',
			component: () => import(/* webpackChunkName: "Contact" */ './views/Contact/Contact.vue'),
			meta: { title: `Contact - ${title}` }
		},
	]
})

router.beforeEach((to, from, next) => {
	document.title = to.meta.title
	next()
})

export default router
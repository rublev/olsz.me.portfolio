import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import gallery from './modules/gallery'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		gallery
	},
	plugins: [
		createLogger(),
	],
	strict: process.env.NODE_ENV !== 'production',
})
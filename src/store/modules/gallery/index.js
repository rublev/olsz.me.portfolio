import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const state = {
	open: false,
	index: 0,
	images: null,
}

export default {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
}
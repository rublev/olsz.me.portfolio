const open = ({ commit }, { picIndex, images }) => {
	commit('OPEN', { picIndex, images })
}

const close = ({ commit }) => {
	commit('CLOSE')
}

const previous = ({ commit }) => {
	commit('PREVIOUS')
}

const next = ({ commit }) => {
	commit('NEXT')
}

export default {
	open,
	close,
	previous,
	next,
}
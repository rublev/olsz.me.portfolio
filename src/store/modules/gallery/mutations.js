const OPEN = (state, payload) => {
	state.open = !state.open
	state.index = payload.picIndex
	state.images = payload.images
}

const CLOSE = (state, payload) => {
	state.open = !state.open
	state.index = 0
	state.images = null
}

const PREVIOUS = (state, payload) => {
	state.index = state.index - 1
}

const NEXT = (state, payload) => {
	state.index = state.index + 1
}

export default {
	OPEN,
	CLOSE,
	PREVIOUS,
	NEXT,
}
/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icons/contact': {
    width: 75,
    height: 51,
    viewBox: '0 0 75 51',
    data: '<path pid="0" d="M4 .5h67a4 4 0 014 4v40a6 6 0 01-6 6H6a6 6 0 01-6-6v-40a4 4 0 014-4zm33.5 27.344L69.336 3.625H5.664L37.5 27.844zM7.125 47.375h60.75a4 4 0 004-4V5.578l-22.46 17.188 13.28 15.039-.39.39-15.43-13.672L37.5 31.75l-9.375-7.227-15.43 13.672-.39-.39 13.28-15.04L3.126 5.579v37.797a4 4 0 004 4z" _fill="#FFF" fill-rule="nonzero"/>'
  }
})

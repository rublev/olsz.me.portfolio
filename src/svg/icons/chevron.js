/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icons/chevron': {
    width: 16,
    height: 29,
    viewBox: '0 0 16 29',
    data: '<path pid="0" d="M.295 27.116a1 1 0 101.41 1.418l13.502-13.412a1 1 0 000-1.418L1.705.29a1 1 0 10-1.41 1.418l12.788 12.704L.295 27.116z" _fill="#FFF" fill-rule="nonzero"/>'
  }
})

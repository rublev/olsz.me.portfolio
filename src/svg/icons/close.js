/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icons/close': {
    width: 20,
    height: 20,
    viewBox: '0 0 20 20',
    data: '<path pid="0" d="M.293.293a1 1 0 011.414 0l7.935 7.935L17.579.293a1 1 0 011.414 1.414l-7.936 7.935 7.936 7.937a1 1 0 01.083 1.32l-.083.094a1 1 0 01-1.414 0l-7.937-7.936-7.935 7.936a1 1 0 11-1.414-1.414l7.935-7.937L.293 1.707A1 1 0 01.21.387z" _fill="#FFF" fill-rule="nonzero"/>'
  }
})

/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'companies/thumbs/brd': {
    width: 40,
    height: 40,
    viewBox: '0 0 40 40',
    data: '<defs><linearGradient x1="-1.924%" y1="100.28%" x2="102.415%" y2="7.494%" id="svgicon_companies_thumbs_brd_a"><stop stop-color="#ED5092" offset="0%"/><stop stop-color="#EA9D26" offset="100%"/></linearGradient><path pid="0" d="M19.429 4.762C19.429 2.286 17.714 0 13.143 0H.953A.943.943 0 000 .952v17.143c0 .534.419.953.952.953h12.381c4.762 0 6.667-2.515 6.667-5.143 0-2.629-2.514-4.572-5.143-4.572 2.629 0 4.572-2.095 4.572-4.571zM8 11.429h5.714c.953 0 1.715.761 1.715 1.714 0 .952-.762 1.714-1.715 1.714H4.571V4.19h8.572c.952 0 1.714.572 1.714 1.524 0 .953-.762 1.524-1.714 1.524H8a.943.943 0 00-.952.952v2.286c0 .534.419.953.952.953z" id="svgicon_companies_thumbs_brd_b"/></defs><g _fill="none" fill-rule="evenodd"><rect pid="1" _fill="url(#svgicon_companies_thumbs_brd_a)" width="40" height="40" rx="4"/><use _fill="#FFF" fill-rule="nonzero" xlink:href="#svgicon_companies_thumbs_brd_b" transform="translate(10 10)"/></g>'
  }
})

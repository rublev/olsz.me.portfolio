/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'companies/thumbs/parcel': {
    width: 40,
    height: 40,
    viewBox: '0 0 40 40',
    data: '<defs><path pid="0" d="M17.913 6.677v.665c0 1.746 0 3.492.08 5.321.082 1.83-.65 3.16-2.197 3.99a82.305 82.305 0 00-4.558 2.661c-1.465.915-3.011.915-4.476 0-1.547-.914-3.012-1.829-4.558-2.66-1.547-.832-2.28-2.162-2.198-3.99V6.676l6.105 3.492s2.686 1.995 6.104-.083l3.418-2.078 2.28-1.33zM6.675.686c1.464-.915 3.01-.915 4.474 0 1.546.915 3.01 1.83 4.556 2.66.895.5 1.545 1.165 1.87 1.996h-.08l-6.59 3.908s-1.79 1.081-3.742 0a126.144 126.144 0 00-2.278-1.247l9.274-5.488-1.383-.831-9.192 5.57L.248 5.343c.407-.748.976-1.413 1.871-1.995A82.25 82.25 0 006.675.686z" id="svgicon_companies_thumbs_parcel_a"/></defs><g _fill="none" fill-rule="evenodd"><rect pid="1" _fill="#000" width="40" height="40" rx="4"/><g transform="translate(11 10)"><mask id="svgicon_companies_thumbs_parcel_b" _fill="#fff"><use xlink:href="#svgicon_companies_thumbs_parcel_a"/></mask><use _fill="#FFF" fill-rule="nonzero" xlink:href="#svgicon_companies_thumbs_parcel_a"/><g mask="url(#svgicon_companies_thumbs_parcel_b)" _fill="#FFF"><path pid="2" d="M0 0h18v20H0z"/></g></g></g>'
  }
})

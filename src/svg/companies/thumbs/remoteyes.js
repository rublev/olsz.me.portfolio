/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'companies/thumbs/remoteyes': {
    width: 40,
    height: 40,
    viewBox: '0 0 40 40',
    data: '<defs><path pid="0" d="M10.677 16.073c2.841-.425 5.023-2.848 5.147-5.712.026-.572.015-.656-.088-.903-.381-.907-1.348-2.007-5.917-2.017-4.57-.01-5.564 1.11-5.946 2.017-.103.247-.113.33-.087.903.118 2.832 2.295 5.272 5.09 5.707.656.105 1.14.105 1.8.005z" id="svgicon_companies_thumbs_remoteyes_a"/></defs><g _fill="none" fill-rule="evenodd"><rect pid="1" _fill="#2E54F9" width="40" height="40" rx="4"/><g transform="matrix(1 0 0 -1 10 30)"><path pid="2" d="M8.722 19.877a9.906 9.906 0 01-4.281-1.569c-.614-.414-.887-.635-1.46-1.201C-.036 14.127-.85 9.553.954 5.692a9.961 9.961 0 013.523-4.087C8.366-.986 13.565-.388 16.825 3.027c.562.587.778.86 1.201 1.521 2.548 3.96 1.96 9.243-1.397 12.559-.815.808-1.537 1.322-2.569 1.836a9.79 9.79 0 01-5.338.934z" _fill="#FFF" fill-rule="nonzero"/><mask id="svgicon_companies_thumbs_remoteyes_b" _fill="#fff"><use xlink:href="#svgicon_companies_thumbs_remoteyes_a"/></mask><use _fill="#2E54F9" fill-rule="nonzero" xlink:href="#svgicon_companies_thumbs_remoteyes_a"/><path pid="3" d="M8.823 13.567c-.557-.116-1.342-.496-1.836-.876a4.603 4.603 0 01-1.76-3.521c-.012-.554.108-.824.494-1.12.804-.625 2.228-.96 4.083-.96 1.538 0 2.62.193 3.526.625.898.438 1.145.875 1.025 1.848-.335 2.736-2.88 4.583-5.532 4.004z" _fill="#879BFF" fill-rule="nonzero" mask="url(#svgicon_companies_thumbs_remoteyes_b)"/></g></g>'
  }
})

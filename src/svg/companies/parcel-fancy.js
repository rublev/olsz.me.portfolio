/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'companies/parcel-fancy': {
    width: 308,
    height: 141,
    viewBox: '0 0 308 141',
    data: '<defs><path pid="0" d="M70.655 25.707v2.561c0 6.722 0 13.443.32 20.485.322 7.042-2.567 12.163-8.667 15.364-6.1 3.2-12.2 6.721-17.98 10.242-5.778 3.521-11.878 3.521-17.657 0-6.1-3.52-11.879-7.041-17.979-10.242-6.1-3.201-8.989-8.322-8.668-15.364V25.707l24.079 13.444s10.594 7.682 24.078-.32l13.484-8.002 8.99-5.122zM26.329 2.641c5.776-3.521 11.873-3.521 17.648 0 6.097 3.521 11.873 7.043 17.97 10.244 3.53 1.92 6.096 4.481 7.38 7.683h-.32L43.014 35.613s-7.06 4.162-14.76 0c-7.702-4.161-8.985-4.802-8.985-4.802L55.85 9.684l-5.455-3.202-36.26 21.448L.98 20.568c1.605-2.881 3.85-5.442 7.38-7.683C14.457 9.684 20.554 6.162 26.33 2.64z" id="svgicon_companies_parcel-fancy_a"/></defs><g _fill="none" fill-rule="evenodd"><circle pid="1" _fill="#000" cx="70.5" cy="70.5" r="70.5"/><g transform="translate(35 32)"><mask id="svgicon_companies_parcel-fancy_b" _fill="#fff"><use xlink:href="#svgicon_companies_parcel-fancy_a"/></mask><use _fill="#FFF" fill-rule="nonzero" xlink:href="#svgicon_companies_parcel-fancy_a"/><g mask="url(#svgicon_companies_parcel-fancy_b)" _fill="#FFF"><path pid="2" d="M0 0h71v77H0z"/></g></g><text font-family="Klavika-Regular, Klavika" font-size="55" _fill="#FFF"><tspan x="176.405" y="84">parcel</tspan></text></g>'
  }
})

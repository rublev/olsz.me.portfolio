/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'logo/head': {
    width: 288,
    height: 288,
    viewBox: '0 0 288 288',
    data: '<defs><path pid="0" d="M191.7 35.9C63.9-35.9 0 0 0 143.7s63.9 179.6 191.7 107.8c127.7-71.9 127.7-143.7 0-215.6zm-5.5 206.4C84 299.8 32.9 271 32.9 156S84 12.3 186.2 69.8c102.2 57.5 102.2 115 0 172.5z" id="svgicon_logo_head_a"/></defs><use _fill="#FFF" fill-rule="nonzero" xlink:href="#svgicon_logo_head_a"/>'
  }
})

// polyfills
import 'es6-promise/auto'
import 'intersection-observer'

import FastClick from 'fastclick'
import Vue from 'vue'
import Vuex from 'vuex'
import * as svgicon from 'vue-svgicon'
import VueScrollTo from 'vue-scrollto'
import VueWait from 'vue-wait'
import { VLazyImagePlugin } from 'v-lazy-image'

import App from '@/containers/App/App.vue'
import router from '@/router'
import store from '@/store'

// uses
	Vue.use(Vuex)
	Vue.use(VueScrollTo, { easing: 'ease', duration: 1000 })
	Vue.use(svgicon, { tagName: 'svgicon' })
	Vue.use(VueWait)
	Vue.use(VLazyImagePlugin)

window.addEventListener('load', () => {
  FastClick.attach(document.body)
}, false);

window.history.scrollRestoration = 'manual'

Vue.config.productionTip = false

var vm = new Vue({
	router,
	store,
	render: h => h(App),
	wait: new VueWait(),
}).$mount('#app')

router.beforeEach((to, from, next) => {
	vm.$wait.start('router')
	setTimeout(function(){
		next()
		setTimeout(function(){
			vm.$wait.end('router')
		}, 50)
	}, 500)
})